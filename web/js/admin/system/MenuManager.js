﻿var main = null;

$(function () {
    main = new Main();
    main.init();

    $.extend($.fn.validatebox.defaults.rules, {
            ip: {
            validator: function (value) {
                if (!isIP(value)) {
                    return false;
                }
                return true;
                },
                    message: "IP地址不符合规范"
                    }
    });

});

//将ip转换为数字
function ip2long(ip) {
    var arr = ip.split(".");
    return arr[0]* 256 * 256*256 +arr[1]* 256 * 256 +arr[2]* 256 + arr[3]*1;
    }

function isIP(s) //by zergling 
    {
    var patrn = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
        if (!patrn.exec(s)) return false
        return true
    }
//主操作类
function Main() {
    this.dlg = null;
    this.dg = null;
    this.treeLeft = null;
    this.dlg_1 = null;
    this.init = function () {
        this.dg = new Dg();
        this.dlg = new Dlg();
        this.treeLeft = new LeftTree();
        this.dg.init();
        this.dlg.init();
        this.treeLeft.init();

        var me = this;
        $("#d_tb .btn_add").click(function () {
            me.dlg.showAdd();
        });
        $("#d_tb .btn_modify").click(function () {
            me.dlg.showModify();
        });
        $("#d_tb .btn_del").click(function () {
            me.dg.del();
        });
    };
}
//主表格类,继承Dg_Method的方法
function Dg() {
    this.status = 0;
    this.init = function () {
        this.searchForm = $("#searchForm");
        this.d_tb = "#d_tb";
        this.url = "/service/MenuService.ashx";
        this.action = "query";
        this.pagination = false;
        this.getColumns = function () {
            var columns = new Array();
            columns.push(
               { field: "id", title: "ID", checkbox: true },
               { field: "text", title: "名称", width: 100 },
               { field: "url", title: "地址", width: 200 },
               { field:"iconCls",title:"iconCls",width:50},
               {field:"icon",title:"icon",width:50},
               {field:"sequence",title:"sequence",width:30}
            );
                    return columns;
        };

        Dg_Method.init.call(this);

      };
}
//主对话框类,继承Dg_Method的方法
function Dlg() {
    this.url = "/service/MenuService.ashx";
    this.init = Dlg_Method.init;
    this.showAdd = function () {
        Dlg_Method.showAdd.call(this, arguments);
        var fj = $("#searchForm").serializeJson();
        this.mainForm.form("load", { pId: fj.pId })
    };
    this.showModify = function() {
         Dlg_Method.showModify.call(this);
    };
    this.submit = function () {
        var me = this;
        Dlg_Method.submit.call(this, function () {
           me.dlg.dialog("close");
           me.refresh();
           main.treeLeft.init();
        });
    };

}


///左边树操作类
function LeftTree() {
    this.tree = $("#tree_left");
    this.url = "/service/MenuService.ashx?action=query";
    this.getTreeObj = function () {
        var treeObj = $.fn.zTree.getZTreeObj("tree_left");
        return treeObj;
    };
    this.setting = {
        async: {
            enable: true,
            url: this.url,
            dataFilter: function (treeId, parentNode, rList) {
                rList.push({ text: "主菜单",id:0,open:true });
                return rList;
            }
        },
        data: {
            key: {
                name: "text",
                url: "xUrl"
            },
            simpleData: {
                enable: true,
                idKey: "id",
                pIdKey: "pId"
            }
        },
        callback: {
            beforeAsync: function () {
                $("#panel_west").showLoading();
            },
            onAsyncError: function () {
                $("#panel_west").hideLoading();
            },
            onClick: function (event, treeId, node) {
                if (!node.pId) {
                    $("#searchForm").form("load", { pId: node.id });
                    main.dg.init();
                }
               
            },
            onAsyncSuccess: function (event, treeId) {
                $("#panel_west").hideLoading();
                var treeObj = $.fn.zTree.getZTreeObj(treeId);
                var nodes = treeObj.getNodes();
                var node = nodes[0];
                treeObj.selectNode(node);


            }

        }
    };
    this.init = function () {
        $.fn.zTree.init(this.tree, this.setting);
    };

}

function onFileIconChange(f) {
    if (f.value) {
        ajaxFileUpload();
    }
}

function ajaxFileUpload() {
    $("#img_icon").attr("src", "/images/loading.gif");
    $.ajaxFileUpload
    (
        {
            url: '/service/SourceService.ashx?action=UploadImage', //用于文件上传的服务器端请求地址
            secureuri: false, //是否需要安全协议，一般设置为false
            fileElementId: 'file_icon', //文件上传域的ID
            dataType: 'json', //返回值类型 一般设置为json
            success: function (data, status)  //服务器成功响应处理函数
            {
                //           $.messager.progress("close");
                if (data.success) {
                    $("#text_icon").val(data.imgurl);
                } else {
                    $.messager.alert("上传图片失败", data.msg, "error");
                }
                $("#img_icon").attr("src", $("#text_icon").val());
            },
            error: function (data, status, e)//服务器响应失败处理函数
            {
                //          $.messager.progress("close");
                $("#img_icon").attr("src", $("#text_icon").val());
                $.messager.alert("错误", "出错了,请刷新后重试", "error");
            }
        }
    )
    return false;
}


