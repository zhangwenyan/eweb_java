<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>eweb</title>
    <%@include file="../inc/_header.jsp"%>
</head>
<body>
<c:set var="page_index" value="article.index"></c:set>
<%@include file="../inc/_nav.jsp"%>
<div class="container">
    <ol class="breadcrumb">
        <li>当前位置：</li>
        <li><a href="/face/Index">首页</a></li>
        <li>文章</li>
        </li>
    </ol>


    <span style="border: 1px solid green;padding: 10px;position: fixed;left: 10px;top: 80px;">
        <a href="/face/article/Add">发表文章</a>
    </span>
    <h3>最新文章</h3>
    <ul class="list-group">
        <c:forEach items="${list}" var="article">
            <a target="_blank" href="/face/article/${article.id}.html" class="list-group-item">
                <h3 class="list-group-item-heading">
                        ${article.title}
                </h3>
                <p class="list-group-item-text" style="text-align: right;">
                    ${article.user.nickname} 发表于${article.createTime}
                </p>
            </a>
        </c:forEach>

    </ul>


</div>
</body>
</html>
