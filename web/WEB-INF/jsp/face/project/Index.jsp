<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>eweb</title>
    <%@include file="../inc/_header.jsp"%>
</head>
<body>
<c:set var="page_index" value="project.index"></c:set>
<%@include file="../inc/_nav.jsp"%>
<div class="container">
    <ol class="breadcrumb">
        <li>当前位置：</li>
        <li><a href="/face/Index">首页</a></li>
        <li>项目</li>
        </li>
    </ol>

    <span style="border: 1px solid green;padding: 10px;position: fixed;left: 10px;top: 80px;">
        <a href="/face/project/Add">增加项目</a>
    </span>
    <h3>最新项目</h3>
    <ul class="list-group">
        <c:forEach items="${list}" var="project">
            <a target="_blank" href="/face/project/${project.id}" class="list-group-item">
                <h3 class="list-group-item-heading">
                        ${project.name}
                </h3>
                <p class="list-group-item-text">
                        ${project.description}
                       <%-- ${article.user.nickname} 发表于${article.createTime}--%>
                </p>
            </a>
        </c:forEach>

    </ul>



</div>
</body>
</html>
