<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${entity.name}</title>
    <%@include file="../inc/_header.jsp"%>
</head>
<body>

<div class="container">
    <div class="page-header">
        <h1>${entity.name}

        </h1>
        <div style="color: gray;">
            ${entity.description}
        </div>
    </div>
    <div class="col-lg-6">
            GIT地址:
            <div class="input-group">
                <input type="text" class="form-control"  value="${entity.gitUrl}">
                   <span class="input-group-btn">
                      <button class="btn btn-default" type="button">
                         复制
                      </button>
                   </span>
            </div>

            SVN地址:
            <div class="input-group">
                <input type="text" class="form-control"  value="${entity.svnUrl}">
                   <span class="input-group-btn">
                      <button class="btn btn-default" type="button">
                         复制
                      </button>
                   </span>
            </div>
    </div>



</div>


</body>
</html>
