<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>eweb</title>
    <%@include file="../inc/_header.jsp"%>
    <script src="/js/base.js" type="text/javascript"></script>
</head>
<body>
<c:set var="page_index" value="project.add"></c:set>
<%@include file="../inc/_nav.jsp"%>
<div class="container">
    <ol class="breadcrumb">
        <li>当前位置：</li>
        <li><a href="/face/Index">首页</a></li>
        <li><a href="/face/article/Index">项目</a></li>
        <li>添加项目</li>
    </ol>

    <form id="fm" onsubmit="doSubmit();return false;">
        <div class="form-group">
            <label for="name">名称</label>
            <input name="name" type="text" class="form-control" id="name" placeholder="请输入名称">
        </div>
        <div class="form-group">
            <label for="description">描述</label>
            <textarea name="description" multiple="multiple" type="text" class="form-control" id="description" placeholder="请输入描述"></textarea>
        </div>

        <div class="form-group">
            <label for="gitUrl">Git地址</label>
            <input name="gitUrl" type="text" class="form-control" id="gitUrl" placeholder="请输入Git地址">
        </div>
        <div class="form-group">
            <label for="svnUrl">SVN地址</label>
            <input name="svnUrl" type="text" class="form-control" id="svnUrl" placeholder="请输入SVN地址">
        </div>
<%--        <div class="form-group">
            <label for="startTime">项目开始时间</label>
            <input type="text" class="form-control" id="startTime" placeholder="请输入SVN地址">
        </div>--%>
        <button type="submit" class="btn btn-default">确认</button>
    </form>
</div>

<script>

    function doSubmit(){
        var data = $("#fm").serializeJson();
        data.action = "add";
        console.log(data);
        $("body").showLoading();
        $.post("/face/project_do",data,function(result){
            if(result.success === false){
                alert(result.msg);
            }else{
                alert("添加项目成功");
            }
        },"json").error(onError).complete(function(){
            $("body").hideLoading();
        });

    }

</script>



</body>
</html>
