<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar">asd</span>
                <span class="icon-bar">ff</span>
                <span class="icon-bar">aa</span>
            </button>
            <a class="navbar-brand" href="/face/Index">WWW.APPX.TOP</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li ${page_index=="index"?"class=\"active\"":""}><a href="/face/Index">首页<span class="sr-only">(current)</span></a></li>
                <li ${page_index.startsWith("article.")?"class=\"active\"":""}><a href="/face/article/Index">文章</a></li>
                <li ${page_index.startsWith("project.")?"class=\"active\"":""}><a href="/face/project/Index">项目</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">更多<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
            </ul>
            <form class="navbar-form navbar-left" role="search" action="/face/article/Search">
                <div class="form-group">
                    <input type="text" name="kw" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">搜索</button>
            </form>
                <ul class="nav navbar-nav navbar-right">
                    <c:choose>
                        <c:when test="${user!=null}">
                            <li class="dropdown ${page_index.startsWith("user.")?"active":""}">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">${user.nickname}(已登录)<span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li ${page_index=="user.index"?"class='active'":""}><a href="/face/user/Index">我的主页</a></li>
                                    <c:if test="${user!=null && user.username=='admin'}">
                                        <li><a target="_blank" href="/admin/Index">后台</a></li>
                                    </c:if>
                                    <li><a href="#">Something else here</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/admin/system/Logout?redirectURL=/">退出</a></li>
                                </ul>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="/admin/system/Login?redirectURL=/face/">登录</a></li>
                        </c:otherwise>
                    </c:choose>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>


