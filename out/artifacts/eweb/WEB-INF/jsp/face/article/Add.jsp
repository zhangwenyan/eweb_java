<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>eweb</title>
    <%@include file="../inc/_header.jsp"%>
    <script src="/js/base.js" type="text/javascript"></script>


    <script type="text/javascript" charset="utf-8" src="/ue/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="/ue/ueditor.all.min.js"> </script>
    <!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
    <!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
    <script type="text/javascript" charset="utf-8" src="/ue/lang/zh-cn/zh-cn.js"></script>

    <style type="text/css">
        #title{
            width:300px;
            margin-bottom: 5px;
        }
    </style>
</head>
<body>
<c:set var="page_index" value="article.add"></c:set>
<%@include file="../inc/_nav.jsp"%>
<div class="container">
    <ol class="breadcrumb">
        <li>当前位置：</li>
        <li><a href="/face/Index">首页</a></li>
        <li><a href="/face/article/Index">文章</a></li>
        <li>添加文章</li>
    </ol>
    <form onsubmit="doSubmit();return false;">
        <input type="hidden" name="action" value="add" />
            <input  type="text" class="form-control" id="title"  placeholder="请输入标题">
            <script id="editor" type="text/plain" style="width:1024px;height:500px;"></script>
            <button type="button" class="btn btn-default" onclick="doSubmit()">提交</button>
    </form>
</div>



<script>
    var ue = UE.getEditor('editor');

function doSubmit(frm){
    var data = {
        action:"add",
        title:$("#title").val(),
        text:UE.getEditor('editor').getContent()
    };
    if(data.title.length == 0){
        alert("请输入标题");
        return;
    }
    $("body").showLoading();
    $.post("/face/article_do",data,function(result) {
            if(result.success === false){
                alert(result.msg);
            }else{
                alert("添加文章成功");
            }
        },"json").error(onError).complete(function(){
        $("body").hideLoading();
    });

}
</script>


</body>
</html>
