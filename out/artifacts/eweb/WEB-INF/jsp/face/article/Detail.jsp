<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${entity.title}</title>
    <%@include file="../inc/_header.jsp"%>
</head>
<body>

<div class="container">
    <div class="page-header">
        <h1>${entity.title}
        </h1>
    </div>
    <div style="text-align: right;color: gray;">
        ${entity.user.nickname} 发表于 ${entity.createTime}
    </div>

    <p>${entity.text}</p>
</div>


</body>
</html>
