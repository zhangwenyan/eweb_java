<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>eweb</title>
    <%@include file="../inc/_header.jsp"%>
</head>
<body>
<c:set var="page_index" value="article.search"></c:set>
<%@include file="../inc/_nav.jsp"%>
<div class="container">
    <ol class="breadcrumb">
        <li>当前位置：</li>
        <li><a href="/face/Index">首页</a></li>
        <li><a href="/face/article/Index">文章</a></li>
        <li>文章搜索</li>
    </ol>

    <h3>文章搜索</h3>
    <ul class="list-group">
        <c:forEach items="${list}" var="article">
            <a target="_blank" href="/face/article/${article.id}.html" class="list-group-item">
                <h3 class="list-group-item-heading">
                        ${article.title}
                </h3>
                <p class="list-group-item-text" style="text-align: right;">
                        ${article.user.nickname} 发表于${article.createTime}
                </p>
            </a>
        </c:forEach>
    </ul>
</div>
</body>
</html>
