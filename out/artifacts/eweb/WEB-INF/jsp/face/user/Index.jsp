<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>eweb</title>
    <%@include file="../inc/_header.jsp"%>
</head>
<body>
<c:set var="page_index" value="user.index"></c:set>
<%@include file="../inc/_nav.jsp"%>
<div class="container">
    用户名:${user.username}
    <br>
    昵称:${user.nickname}
    <br>
    邮箱:${user.email}
</div>
</body>
</html>
