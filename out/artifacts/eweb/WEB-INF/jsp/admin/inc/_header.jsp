<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<link href="/css/style.css" rel="stylesheet" type="text/css">
<link href="/js/jquery-easyui/themes/default/easyui.css" rel="stylesheet" type="text/css" />
<link href="/js/jquery-easyui/themes/icon.css" rel="stylesheet" type="text/css" />
<script src="/js/jquery-1.8.3.js" type="text/javascript"></script>
<script src="/js/jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
<script src="/js/jquery-easyui/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
<link href="/js/zTree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css"/>
<script src="/js/zTree/js/jquery.ztree.all.js" type="text/javascript"></script>
<link href="/js/showLoading.css" rel="stylesheet" />
<script src="/js/jquery.showLoading.min.js" type="text/javascript"></script>
<script src="/js/admin/base.js" type="text/javascript"></script>
