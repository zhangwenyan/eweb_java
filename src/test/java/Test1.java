/**
 * Created by john on 2016/4/24.
 */
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.junit.*;

import java.util.List;

public class Test1 {
    private static final SessionFactory ourSessionFactory;
    private static final ServiceRegistry serviceRegistry;
    private static Session session;
    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            ourSessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }


    @Before
    public void before(){
        System.out.println("before");
        session = ourSessionFactory.openSession();
    }
    @After
    public void after(){
        System.out.println("after");
        session.close();
    }
  /*  @Test
    public void test1(){
       *//* System.out.println("test1");
        Query query = session.createQuery("select new ArticleEntity (id,title) from ArticleEntity");
        List list = query.list();*//*


    }*/

}
