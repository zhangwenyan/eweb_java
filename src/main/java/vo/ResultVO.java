package vo;

import java.util.HashMap;

/**
 * Created by john on 2016/4/24.
 */
public class ResultVO extends HashMap<String,Object> {
    public ResultVO(){
    }
    public ResultVO(boolean success,String msg){
        this.put("success",success);
        this.put("msg",msg);
    }

    public static ResultVO success(){
        ResultVO result = new ResultVO();
        result.put("success",true);
        return result;
    }
    public static ResultVO error(String msg){
        return new ResultVO(false,msg);
    }
    public static ResultVO error(Exception ex){return error(ex.getMessage());}

    public static ResultVO data(){
        return new ResultVO();
    }

    public static ResultVO data(String key,Object value){
        return data().p(key,value);
    }

    public ResultVO p(String key,Object value){
        this.put(key,value);
        return this;
    }

}
