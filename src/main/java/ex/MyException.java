package ex;

/**
 * Created by john on 2016/4/24.
 */
public class MyException extends RuntimeException {
    public MyException(String msg){
        super(msg);
    }
    public MyException(Exception ex){
        super(ex);
    }

}
