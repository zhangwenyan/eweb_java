package utils;

import java.sql.Timestamp;

/**
 * Created by john on 2016/4/30.
 */
public class DateTime {
    public static Timestamp now(){
        return new Timestamp(System.currentTimeMillis());
    }
}
