package interceptor;

import ex.MyException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import vo.ResultVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by john on 2016/4/24.
 */
public class MySimpleMappingExceptionResolver extends SimpleMappingExceptionResolver {

    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        try {
            if (ex instanceof MyException) {
                PrintWriter writer = response.getWriter();
                writer.write(com.alibaba.fastjson.JSONObject.toJSONString(ResultVO.error(ex)));
                writer.flush();
                return null;
            }
        }catch (IOException ex1){

        }
        System.out.println("异常："+ex.getMessage());
        return super.doResolveException(request, response, handler, ex);
    }
}
