package com.dao;

import entity.RoleEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by john on 2016/4/30.
 */
@Repository
public class RoleDao extends BaseDao<RoleEntity>{

    public List<RoleEntity> list(){
        return find("from RoleEntity");
    }

}
