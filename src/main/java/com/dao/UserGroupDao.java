package com.dao;

import entity.UserGroupEntity;
import ex.MyException;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 2016/4/24.
 */
@Repository
public class UserGroupDao extends BaseDao<UserGroupEntity>{
    public List<UserGroupEntity> query() {
        return find("from UserGroupEntity");
    }



    public void modify(UserGroupEntity userGroupEntity) {
        UserGroupEntity oldUserGroupEntity = get(userGroupEntity.getId());
        if(oldUserGroupEntity == null){
            throw new MyException("该记录不存在或已被删除");
        }
        oldUserGroupEntity.setName(userGroupEntity.getName());
        oldUserGroupEntity.setComment(userGroupEntity.getComment());
        oldUserGroupEntity.setIcon(userGroupEntity.getIcon());
        update(oldUserGroupEntity);
    }

    public void add(UserGroupEntity userGroupEntity) {
        save(userGroupEntity);
    }

    public void delByIds(String ids) {
        List<Integer> idList = new ArrayList<Integer>();
        for(String idStr :ids.split(",")){
            idList.add(Integer.parseInt(idStr));
        }
        for(int id : idList){
            UserGroupEntity userGroupEntity = get(id);
            String sql = "select count(*) from UserEntity where status=1 and userGroupId="+id;
            Long count = (Long)find(sql).get(0);
            if(count>0){
                throw new MyException("<strong>"+userGroupEntity.getName()+"</strong>用户组下有用户存在");
            }
            delete(userGroupEntity);
        }
    }

}
