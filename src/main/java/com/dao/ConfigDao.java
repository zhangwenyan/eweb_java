package com.dao;

import entity.ConfigEntity;
import ex.MyException;
import org.springframework.stereotype.Repository;
import utils.StringUtil;
import vo.Pager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 2016/5/2.
 */
@Repository
public class ConfigDao extends BaseDao<ConfigEntity> {
    public Pager<ConfigEntity> queryPage(Pager pager,ConfigEntity configEntity) {
        String sql = "from ConfigEntity where 1=1 ";
        List<Object> paramValues = new ArrayList<Object>();
        if(configEntity != null){
            if(!StringUtil.isNullOrEmpty(configEntity.getName())){
                sql += " and name like ?";
                paramValues.add("%"+configEntity.getName()+"%");
            }
            if(!StringUtil.isNullOrEmpty(configEntity.getComment())){
                sql += " and comment like ?";
                paramValues.add("%"+configEntity.getComment()+"%");
            }
        }
        return pagedQuery(pager,sql,paramValues.toArray());
    }

    public void add(ConfigEntity configEntity){
        save(configEntity);
        long c = count("select count(*) from ConfigEntity where name=?",configEntity.getName());
        if(c>1){
            throw new MyException("已有相同名称配置");
        }
    }
    public void modify(ConfigEntity configEntity){
        update(configEntity);
        long c = count("select count(*) from ConfigEntity where name=?",configEntity.getName());
        if(c>1){
            throw new MyException("已有相同名称配置");
        }
    }
    public void delByIds(String ids){
        List<Integer> idList = new ArrayList<Integer>();
        for(String idStr :ids.split(",")){
            idList.add(Integer.parseInt(idStr));
        }
        for(int id : idList) {
            ConfigEntity configEntity = get(id);
            if(configEntity == null){
                continue;
            }
            delete(configEntity);
        }
    }

}
