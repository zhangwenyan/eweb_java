package com.dao;

import entity.ArticleEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by john on 2016/4/29.
 */
@Repository
public class ArticleDao extends BaseDao<ArticleEntity> {

    public List<ArticleEntity> list() {
        return find("from ArticleEntity");
    }
    public List<ArticleEntity> list_search(String kw) {
        return find("from ArticleEntity where title like ? order by id desc","%"+kw+"%");
    }

    public List<ArticleEntity> list_index(){
        Query query = createQuery("from ArticleEntity order by id desc");
        query.setFirstResult(0);
        query.setMaxResults(20);
        return query.list();
    }


    public void add(ArticleEntity articleEntity) {
        save(articleEntity);
    }

    public ArticleEntity findById(Integer id) {
        return get(id);
    }
}
