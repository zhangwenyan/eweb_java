package com.dao;

import entity.ArticleEntity;
import entity.ProjectEntity;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by john on 2016/5/17.
 */
@Repository
public class ProjectDao extends BaseDao<ProjectEntity> {
    public void add(ProjectEntity entity){
        save(entity);
    }

    public List<ProjectEntity> list_index() {
        Query query = getSession().createQuery("from ProjectEntity order by id desc");
        query.setFirstResult(0);
        query.setMaxResults(10);
        return query.list();
    }

    public ProjectEntity findById(Integer id) {
        return get(id);
    }
}
