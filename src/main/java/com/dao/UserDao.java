package com.dao;

import entity.UserEntity;
import entity.UserGroupEntity;
import ex.MyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import utils.DateTime;
import vo.Pager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by john on 2016/4/24.
 */
@Repository
public class UserDao extends BaseDao<UserEntity> {

    @Autowired
    private UserGroupDao userGroupDao;

    private UserEntity findByName(String username){
        List<UserEntity> userEntityList = find("from UserEntity where username=?",username);
        if(userEntityList.size()!=0){
            return userEntityList.get(0);
        }
        else{
            return null;
        }
    }
    private UserEntity findByUsernameAndStatusIsOne(String username){
        List<UserEntity> userEntityList = find("from UserEntity where username=? and status=1",username);
        if(userEntityList.size()!=0){
            return userEntityList.get(0);
        }
        else{
            return null;
        }
    }

    public UserEntity login(String username,String password){
        UserEntity userEntity = findByName(username);
        if(userEntity == null){
            throw new MyException("该用户不存在");
        }
        switch (userEntity.getStatus()){
            case 1:
                break;
            case 2:
                throw new MyException("该用户已被删除,不能登录");
            default:
                throw new MyException("用户状态不对,不能登录");
        }


        if(!userEntity.getPassword().equals(password)){
            throw new MyException("密码错误");
        }
        return userEntity;
    }

    public Pager<UserEntity> queryPage(Pager pager,int userGroupId) {
        String sql = "from UserEntity where status=1 ";
        if(userGroupId != 0){
            sql += " and userGroupId="+userGroupId;
        }
        return pagedQuery(pager,sql);
    }



    public void resetPasswordByIds(String ids, String newPassword) {
        List<Integer> idList = new ArrayList<Integer>();
        for(String idStr : ids.split(",")){
            idList.add(Integer.parseInt(idStr));
        }

        if(newPassword==null || newPassword.length() == 0){
            newPassword= "888888";
        }
        int i = getSession().createQuery("update UserEntity set password=:newPassword where id in :ids").setString("newPassword",newPassword).setParameterList("ids",idList).executeUpdate();
        System.out.println(i);
    }

    public void delByIds(String ids) {
        List<Integer> idList = new ArrayList<Integer>();
        for(String idStr :ids.split(",")){
            idList.add(Integer.parseInt(idStr));
        }
        for(int id : idList) {
            UserEntity userEntity = get(id);
            if(userEntity == null){
                continue;
            }
            if("admin".equals(userEntity.getUsername())){
                throw new MyException("不能删除<strong>admin</strong>用户");
            }
            userEntity.setStatus(2);
        }
    }

    public void add(UserEntity userEntity) {
        if(findByUsernameAndStatusIsOne(userEntity.getUsername())!=null){
            throw new MyException("用户名已存在");
        }
        userEntity.setCreateTime(DateTime.now());
        userEntity.setStatus(1);
        save(userEntity);
    }

    public void modify(UserEntity userEntity) {
        UserEntity oldUserEntity = get(userEntity.getId());
        if(oldUserEntity==null){
            throw new MyException("该用户不存在");
        }
        //UserGroupEntity userGroupEntity = userGroupDao.get(user)

        oldUserEntity.setEmail(userEntity.getEmail());
        oldUserEntity.setNickname(userEntity.getNickname());
        oldUserEntity.setRoleIds(userEntity.getRoleIds());
   //     oldUserEntity.setPassword(userEntity.getPassword());
        oldUserEntity.setSex(userEntity.getSex());
        oldUserEntity.setUserGroup(userEntity.getUserGroup());

    }
}
