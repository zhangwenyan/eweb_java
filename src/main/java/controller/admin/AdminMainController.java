package controller.admin;

import controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by john on 2016/4/24.
 */

@Controller
public class AdminMainController extends BaseController {



    @RequestMapping({"/admin/","/admin/Index"})
    public String index(HttpSession session){
        Object userObj = session.getAttribute(SESSION_USER_KEY);
        if(userObj == null){
            return "admin/Login";
        }
        return "admin/Index";
    }

    @RequestMapping("/admin/system/Login")
    public String login(){
        return "admin/Login";
    }

    @RequestMapping({"/admin/system/Logout"})
    public String login(HttpSession session, String redirectURL, HttpServletResponse response){
        session.removeAttribute(SESSION_USER_KEY);
        if(redirectURL == null){
            return "admin/Login";
        }else {
            try {
                response.sendRedirect(redirectURL);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    @RequestMapping("/admin/Home")
    public String home(){
        return "admin/Home";
    }


    @RequestMapping("/admin/system/RoleManager")
    public String roleManager(){
        return "admin/system/RoleManager";
    }

    @RequestMapping("/admin/system/UserGroupManager")
    public String userGroupManager(){
        return "admin/system/UserGroupManager";
    }

    @RequestMapping("/admin/system/ConfigManager")
    public String configManager(){
        return "admin/system/ConfigManager";
    }



    @RequestMapping("/admin/system/ModifyPassword")
    public String modifyPassword(){
        return "admin/system/ModifyPassword";
    }




}
