package controller.admin;

import controller.BaseController;
import com.dao.MenuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by john on 2016/4/24.
 */
@Controller
@RequestMapping("/admin/system/Menu")
public class MenuController extends BaseController {
    @Autowired
    private MenuDao menuDao;

    @RequestMapping(params = "action=myMenuTree")
    @ResponseBody
    public Object myMenuTree(){
        System.out.println("myMenuTree");
        return menuDao.myMenuTree(null);
    }
}
