package controller.admin;

import controller.BaseController;
import com.dao.UserDao;
import entity.UserEntity;
import form.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import vo.Pager;
import vo.ResultVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 2016/4/24.
 */
@Controller
@RequestMapping("/admin/system/User")
public class UserController extends BaseController<UserEntity> {

    @Autowired
    private UserDao userDao;

    @RequestMapping(params = "action=login")
    @ResponseBody
    public Object login(UserForm userForm, HttpSession session){
        UserEntity userEntity = userDao.login(userForm.getUsername(), userForm.getPassword());
        session.setAttribute("user",userEntity);
        return ResultVO.success();
    }

    @RequestMapping(params = "action=getCurUser")
    @ResponseBody
    public Object getCurUser(HttpSession session){
        UserEntity userEntity = getCurSessionUser(session);
        return ResultVO.success().p("nickname",userEntity.getUsername());
    }

    @RequestMapping(params = "action=queryPage")
    @ResponseBody
    public Object queryPage(Pager pager,int userGroupId){
        List<Object> rList = new ArrayList<Object>();
        Pager<UserEntity> pager2 = userDao.queryPage(pager,userGroupId);

        for (UserEntity ue:pager2.getRows() ) {
            rList.add(ResultVO.data()
                    .p("id",ue.getId())
                    .p("username",ue.getUsername())
                    .p("nickname",ue.getNickname())
                    .p("createTime",ue.getCreateTime())
                    .p("lastTime",ue.getLastTime())
                    .p("email",ue.getEmail())
                    .p("sex",ue.getSex())
                    .p("userGroupName",ue.getUserGroup()==null?null:ue.getUserGroup().getName())
            );
        }
        Pager p = pager2;
        p.setRows(rList);
        return p;
    }

    @RequestMapping(params = "action=add")
    @ResponseBody
    public Object add(HttpServletRequest request){
        userDao.add(requestEntity(request));
        return ResultVO.success();
    }

    @RequestMapping(params = "action=modify")
    @ResponseBody
    public Object modify(HttpServletRequest request){
        userDao.modify(requestEntity(request));
        return ResultVO.success();
    }

    @RequestMapping(params = "action=delByIds")
    @ResponseBody
    public Object delByIds(String ids){
        userDao.delByIds(ids);
        return ResultVO.success();
    }



    @RequestMapping(params = "action=resetPasswordByIds")
    @ResponseBody
    public Object resetPasswordByIds(String ids,String newPassword){
        userDao.resetPasswordByIds(ids,newPassword);
        return ResultVO.success();
    }


}
