package controller.admin;

import controller.BaseController;
import com.dao.UserGroupDao;
import entity.UserGroupEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import vo.ResultVO;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 2016/4/24.
 */
@Controller
@RequestMapping("/admin/system/UserGroup")
public class UserGroupController extends BaseController<UserGroupEntity> {
    @Autowired
    private UserGroupDao userGroupDao;

    @RequestMapping(params = "action=tree")
    @ResponseBody
    public Object tree(){
        List<UserGroupEntity> list = new ArrayList<UserGroupEntity>();
     //   list.add(ResultVO.data().p("name","全部").p("id",0));
        list.addAll(userGroupDao.query());
        List<Object> rList = new ArrayList<Object>();
        rList.add(ResultVO.data().p("name","全部").p("entity",ResultVO.data().p("flag",2).p("id",0)));
        for (UserGroupEntity userGroupEntity:list) {
            rList.add(ResultVO.data().p("name",userGroupEntity.getName()).p("icon",userGroupEntity.getIcon()).p("entity",ResultVO.data().p("flag",2).p("id",userGroupEntity.getId())));
        }
        return ResultVO.data().p("name","用户分组").p("open",true).p("children",rList).p("entity",ResultVO.data().p("flag",1));
    }

    @RequestMapping(params = "action=query")
    @ResponseBody
    public Object query(){
        List<Object> rList =new ArrayList<Object>();
        for(UserGroupEntity ug :userGroupDao.query()){
            rList.add(ResultVO.data()
                    .p("id",ug.getId())
                    .p("name",ug.getName())
                    .p("createTime",ug.getCreateTime())
                    .p("icon",ug.getIcon())
                    .p("open",ug.getOpen())
                    .p("comment",ug.getComment())
            );
        }
        return rList;
    }



    @RequestMapping(params = "action=add")
    @ResponseBody
    public Object add(HttpServletRequest request){
        UserGroupEntity userGroupEntity = requestEntity(request);
        userGroupDao.add(userGroupEntity);
        return ResultVO.success();
    }

    @RequestMapping(params = "action=delByIds")
    @ResponseBody
    public Object delByIds(String ids){
        userGroupDao.delByIds(ids);
        return ResultVO.success();
    }

    @RequestMapping(params = "action=modify")
    @ResponseBody
    public Object modify(HttpServletRequest request){
        userGroupDao.modify(requestEntity(request));
        return ResultVO.success();
    }







}
