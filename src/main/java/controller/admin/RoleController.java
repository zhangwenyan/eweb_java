package controller.admin;

import com.dao.RoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by john on 2016/4/30.
 */
@Controller
@RequestMapping("/admin/system/Role")
public class RoleController {

    @Autowired
    private RoleDao roleDao;

    @RequestMapping(params = "action=query")
    @ResponseBody
    public Object query(){
        return roleDao.list();
    }
}
