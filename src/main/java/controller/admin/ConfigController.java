package controller.admin;

import com.dao.ConfigDao;
import controller.BaseController;
import entity.ConfigEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import vo.Pager;
import vo.ResultVO;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by john on 2016/5/2.
 */
@Controller
@RequestMapping("/admin/system/Config")
public class ConfigController extends BaseController<ConfigEntity> {

    @Autowired
    private ConfigDao configDao;

    @RequestMapping(params = "action=queryPage")
    @ResponseBody
    public Object queryPage(Pager pager,ConfigEntity configEntity){
        
        return  configDao.queryPage(pager,configEntity);
    }

    @RequestMapping(params = "action=add")
    @ResponseBody
    public Object add(HttpServletRequest request){
        ConfigEntity configEntity = requestEntity(request);
        configDao.add(configEntity);
        return ResultVO.success();
    }

    @RequestMapping(params = "action=modify")
    @ResponseBody
    public Object modify(HttpServletRequest request){
        ConfigEntity configEntity = requestEntity(request);
        configDao.modify(configEntity);
        return ResultVO.success();
    }
    @RequestMapping(params = "action=delByIds")
    @ResponseBody
    public Object delByIds(String ids){
        configDao.delByIds(ids);
        return ResultVO.success();
    }

}
