package controller.face;

import com.dao.ProjectDao;
import controller.BaseController;
import entity.ProjectEntity;
import entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import vo.ResultVO;

import javax.servlet.http.HttpSession;

/**
 * Created by john on 2016/5/17.
 */
@Controller
@RequestMapping("/face/project_do")
public class ProjectController extends BaseController<ProjectEntity> {

    @Autowired
    private ProjectDao projectDao;

    @RequestMapping(params = "action=add")
    @ResponseBody
    public Object add(ProjectEntity entity, HttpSession session){

        //UserEntity userEntity = getCurSessionUser(session);
        projectDao.add(entity);

        return ResultVO.success();
    }

}
