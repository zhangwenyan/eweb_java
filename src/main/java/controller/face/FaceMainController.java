package controller.face;

import com.dao.ProjectDao;
import controller.BaseController;
import com.dao.ArticleDao;
import entity.ArticleEntity;
import entity.ProjectEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * Created by john on 2016/4/29.
 */
@Controller
public class FaceMainController extends BaseController {
    @Autowired
    private ArticleDao articleDao;

    @Autowired
    private ProjectDao projectDao;

    @RequestMapping({"/face/","/face/Index"})
    public String index(HttpSession session){
        return "face/Index";
    }

    @RequestMapping({"/face/article/","/face/article/Index"})
    public ModelAndView article_Index(){
        ModelAndView mav = new ModelAndView();
        mav.addObject("list",articleDao.list_index());
        return mav;
    }


    @RequestMapping({"/face/article/{id}","/face/article/Detail/{id}"})
    public ModelAndView article(@PathVariable Integer id){
        ModelAndView mav = new ModelAndView();
        ArticleEntity entity = articleDao.findById(id);
        if(entity == null){
            mav.setViewName("face/article/notFound");
        }else{
            mav.setViewName("face/article/Detail");
            mav.addObject("entity",entity);
        }
        return mav;
    }

    @RequestMapping("/face/article/Search")
    public ModelAndView article_search(String kw){
        ModelAndView mav = new ModelAndView();
        mav.setViewName("face/article/Search");
        mav.addObject("list",articleDao.list_search(kw));
        return mav;
    }


    @RequestMapping("/face/article/Add")
    public String article_add(HttpSession session){
        if(session.getAttribute(SESSION_USER_KEY) == null){
            //没有登陆
            return "redirect:/admin/system/Login?redirectURL=/face/article/Add";
        }
        return "face/article/Add";
    }

    @RequestMapping({"/face/project/","/face/project/Index"})
    public ModelAndView project_index(HttpSession session){
        ModelAndView mav = new ModelAndView();
        mav.addObject("list",projectDao.list_index());
        return mav;
    }


    @RequestMapping("/face/project/Add")
    public String project_add(HttpSession session){
        if(session.getAttribute(SESSION_USER_KEY) == null){
            //没有登陆
            return "redirect:/admin/system/Login?redirectURL=/face/project/Add";
        }

        return "face/project/Add";
    }

    @RequestMapping({"/face/project/{id}","/face/project/Detail/{id}"})
    public ModelAndView project(@PathVariable Integer id){
        ModelAndView mav = new ModelAndView();
        ProjectEntity entity = projectDao.findById(id);
        if(entity == null){
            mav.setViewName("face/project/notFound");
        }else{
            mav.setViewName("face/project/Detail");
            mav.addObject("entity",entity);
        }
        return mav;
    }




    @RequestMapping("/face/About")
    public String about(){
        return "face/About";
    }


    @RequestMapping({"/face/user/","/face/user/Index"})
    public String user_Index(){
        return "face/user/Index";
    }







}
