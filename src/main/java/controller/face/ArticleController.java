package controller.face;

import controller.BaseController;
import com.dao.ArticleDao;
import entity.ArticleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import utils.DateTime;
import vo.ResultVO;

import javax.servlet.http.HttpSession;

/**
 * Created by john on 2016/4/29.
 */
@Controller
@RequestMapping("/face/article_do")
public class ArticleController extends BaseController {


    @Autowired
    private ArticleDao articleDao;


    @RequestMapping(params = "action=add")
    @ResponseBody
    public Object add(ArticleEntity articleEntity, HttpSession session){
        articleEntity.setUser(getCurSessionUser(session));
        articleEntity.setCreateTime(DateTime.now());
        articleDao.add(articleEntity);
        return ResultVO.success();
    }








}
