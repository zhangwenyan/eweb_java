package entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by john on 2016/5/17.
 */
@Entity
@Table(name = "project", schema = "", catalog = "eweb")
public class ProjectEntity {
    private int id;
    private String name;
    private String description;
    private String gitUrl;
    private String svnUrl;
    private Timestamp startTime;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = true, insertable = true, updatable = true, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, insertable = true, updatable = true, length = 200)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "gitUrl", nullable = true, insertable = true, updatable = true, length = 200)
    public String getGitUrl() {
        return gitUrl;
    }

    public void setGitUrl(String gitUrl) {
        this.gitUrl = gitUrl;
    }

    @Basic
    @Column(name = "svnUrl", nullable = true, insertable = true, updatable = true, length = 200)
    public String getSvnUrl() {
        return svnUrl;
    }

    public void setSvnUrl(String svnUrl) {
        this.svnUrl = svnUrl;
    }

    @Basic
    @Column(name = "startTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProjectEntity that = (ProjectEntity) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (gitUrl != null ? !gitUrl.equals(that.gitUrl) : that.gitUrl != null) return false;
        if (svnUrl != null ? !svnUrl.equals(that.svnUrl) : that.svnUrl != null) return false;
        if (startTime != null ? !startTime.equals(that.startTime) : that.startTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (gitUrl != null ? gitUrl.hashCode() : 0);
        result = 31 * result + (svnUrl != null ? svnUrl.hashCode() : 0);
        result = 31 * result + (startTime != null ? startTime.hashCode() : 0);
        return result;
    }
}
